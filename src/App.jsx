import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  useLocation
} from "react-router-dom";
import './App.css';
// import Kanban from './components/Kanban';
import Main from './components/Main';



function App() {
  //  let query = useQuery();
  return (
    <Router>
      <Route path="/kanbanbranch/:db/:whs">
        <Main />
      </Route>
      <Route path="/kanban/:db">
        <Main />
      </Route>
    </Router>
  );
}

export default App;
