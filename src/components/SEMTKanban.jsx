import React, { Component } from 'react';

import uuidv1 from 'uuid/v1';
import './SEMTKanban.css';
import SEMTKanbanCol from './SEMTKanbanCol';
import axios from 'axios';
import { withRouter } from "react-router";


class SEMTKanban extends Component {
    constructor(props) {
        super(props);
        // Don't call this.setState() here!

        this.state = { data: [] };
        this.getData = this.getData.bind(this);


    }

    getData() {
        let that = this;
        const { match } = this.props;
        axios.get("/prodstatus?db=" + match.params.db + "&whs=" + (match.params.whs||''))
            .then(function (response) {
                // handle success
                that.setState({ data: response.data || [] });
                // console.dir(response.data);
            })
            .catch(function (error) {
                // handle error
                console.log(error);
            })


    }

    componentDidMount() {
        this.getData();

        this.timerID = setInterval(() => {
            // Auto update page every x seconds
            this.getData();
        }, 60 * 1000);
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    render() {
        let cols = this.state.data || [];
        return (
            <div className="SEMTKanban row">
                {
                    cols.map(col => <SEMTKanbanCol key={uuidv1()} data={col} />
                    )
                }
            </div >
        );
    }
}

export default withRouter(SEMTKanban);