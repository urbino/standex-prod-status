import React, { Component } from 'react'; 


import { FontAwesomeIcon } from '@fortawesome/react-fontawesome' 
import { faTruckMoving } from '@fortawesome/free-solid-svg-icons'
 

class SEMTKanbanTile extends Component {
    render() {
        let item = this.props.data || {};
        let r = item.r || '240';
        let g = item.g || '240';
        let b = item.b || '240';
 
        return (
            <div className="SEMTKanban row details" style={{ borderLeft: "30px solid rgb(" + r + ", " + g + ", " + b + ")" }}>


                <p> <strong>{item.toolNumber}</strong> [{item.WONum}] </p>
              <span> {item.Loaded ? <FontAwesomeIcon icon={faTruckMoving} /> : null}</span> 
                <p> {item.SlpInitials} #{item.row_num} </p>

            </div >
        );
    }
}

export default SEMTKanbanTile;