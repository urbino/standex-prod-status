import React, { Component } from 'react';
import SEMTKanbanTile from './SEMTKanbanTile';
import  uuidv1 from 'uuid/v1'; 

class SEMTKanbanGroup extends Component {
    render() {
        let group = this.props.data || {};
        let items = group.items || [];
        return (
            <div className="box SEMTKanban group" >
                <div className="SEMTKanban group-name">{group.name}</div>
                {
                    items.map(item => <SEMTKanbanTile key={uuidv1()} data={item} />)
                }
            </div>
        );
    }
}

export default SEMTKanbanGroup;