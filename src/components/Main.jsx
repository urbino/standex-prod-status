import React, { Component } from 'react';

import { withRouter } from "react-router";

import SEMTKanban from './SEMTKanban';
import standexengraving from '../img/standexengraving.png'; // Tell Webpack this JS file uses this image
import beonesolutions from '../img/beonesolutions.png'; // Tell Webpack this JS file uses this image
import lmsuite from '../img/lmsuite.png'; // Tell Webpack this JS file uses this image

class Main extends Component {


    render() {
        const { match } = this.props;
        return (
            <div className="App">
                <div className="row">
                    <div className="col">
                        <div className="box">
                            <img src={standexengraving} alt="" style={{ "height": "100px" }} />
                        </div>
                    </div>
                    <div className="col">
                        <p>{match.params.db} </p>
                        <div className="box App-title">
                            <p>SEMT - Production Dashboard</p>
                        </div>

                    </div>
                    <div className="col">
                        <div className="box" style={{ textAlign: "end", marginTop: "15pt" }}>
                            <img src={lmsuite} alt="" style={{ "height": "60px" }} />
                            <img src={beonesolutions} alt="" style={{ "height": "50px", paddingLeft: "40pt", marginRight: "40px" }} />
                        </div>
                    </div>
                </div>
                <div className="App-body">
                    <SEMTKanban />
                </div>
            </div>
        );
    }
}

export default withRouter(Main);