import React, { Component } from 'react';
import uuidv1 from 'uuid/v1';
import SEMTKanbanGroup from './SEMTKanbanGroup'; 
class SEMTKanbanCol extends Component {
    constructor(props) {
        super(props);
        this.state = {
            startIX: 1,
            itemsPerPage: 10,
            timePerPage: 20
        };
    }

    componentDidMount() {
        let that = this;
        this.timerID = setInterval(() => {
            // Auto update column every x seconds for a pagging effect
            let col = that.props.data || [];
            let nextStartIX = that.state.startIX + that.state.itemsPerPage;

            if (col.count <= nextStartIX) {
                nextStartIX = 1;
            }
            that.setState({ startIX: nextStartIX });
        }, that.state.timePerPage*1000);
    }

    componentWillUnmount() { 
        clearInterval(this.timerID);
    }


    render() {
        let col = this.props.data || [];
        let groupsReceived = col.groups || [];
        let first_rownum = this.state.startIX;
        let last_rownum = first_rownum + this.state.itemsPerPage;
        if (last_rownum > col.count) last_rownum = col.count;
        //Filter groups with relevant items only
        const groups = [];
        groupsReceived.forEach(grp => {
            let items = [];
            // select items to show.      
            items = grp.items.filter(item => {
                let check = (item.row_num >= first_rownum && item.row_num <= last_rownum)
                return check;
            });
            if (items && items.length === 0) return null;
             
            groups.push({ ...grp, items });
 
        }); 

        return (
            <div className={"SEMTKanban col " + col.name}>
                <div className="SEMTKanban title">{col.date}</div>
                <div className="SEMTKanban count">{"("+first_rownum + "..." + last_rownum + " of " + col.count + " items)"}</div>
                {
                    groups.map(group => <SEMTKanbanGroup key={uuidv1()} data={group} />)
                }
            </div>
        );
    }
}

export default SEMTKanbanCol;